<?php

ini_set('display_errors', 1);

define('ROOT_PATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR));
define('APP_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'app');
define('APP_CONFIG_PATH', APP_PATH . DIRECTORY_SEPARATOR . 'config');
define('WEB_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'web');

require_once APP_PATH . DIRECTORY_SEPARATOR . 'bootstrap.php';
