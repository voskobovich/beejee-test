function sendForm(event) {
    var $button = $(event.target);
    var $form = $button.closest('#comment_form');

    var isPreview = $button.data('preview');
    var data = new FormData($form[0]);

    $.ajax({
        url: $form.attr('action') + '?preview=' + (isPreview ? 1 : 0 ),
        method: 'POST',
        data: data,
        contentType: false,
        processData: false,
        success: function (data) {
            data = JSON.parse(data);

            if (data.errors.length != 0) {
                var allErrors = '';
                for (var index in data.errors) {
                    if (data.errors.hasOwnProperty(index)) {
                        var attributeErrors = data.errors[index];
                        allErrors += attributeErrors.join('\n') + '\n';
                    }
                }
                alert(allErrors);
            } else {
                var $comment = $('#comment_template')
                    .tmpl(data);

                if (isPreview) {
                    $comment.addClass('js-comment_item-preview');
                }

                $('#comments_container > .js-comment_item-preview').last().remove();
                $comment.appendTo('#comments_container');
            }
        }
    });
}


$(document).ready(function () {
    $('.js-comment_send-button').on('click', function (event) {
        sendForm(event);
        return false;
    });
});