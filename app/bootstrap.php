<?php

require_once APP_PATH . DIRECTORY_SEPARATOR . 'AutoLoader.php';
spl_autoload_register(array('AutoLoader', 'loadClass'));

$config = include(APP_CONFIG_PATH . DIRECTORY_SEPARATOR . 'main.php');

$application = \app\core\Application::getInstance($config);
$application->run();