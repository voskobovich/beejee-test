<?php

namespace app\controllers;

use app\core\Controller;
use app\core\HttpException;
use app\core\Request;
use app\models\Comment;


function BasicAuth($user, $pass)
{
    return $user == 'admin' && $pass == '123';
}

if (!BasicAuth($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
    header('WWW-Authenticate: Basic realm="Private Area"');
    header('HTTP/1.0 401 Unauthorized');
    echo "You need to enter a valid username and password.";
    exit;
}


/**
 * Class AdminController
 * @package app\controllers
 */
class AdminController extends Controller
{
    /**
     * Default action
     * @return string
     */
    public function actionIndex()
    {
        $models = Comment::findAll(null, [
            'created_at' => 'DESC'
        ]);

        return $this->render('admin/index', ['models' => $models]);
    }

    /**
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionUpdate($id)
    {
        $model = Comment::findOne([
            'id' => intval($id),
        ]);

        if ($model == null) {
            throw new HttpException('Page not found.');
        }

        if ($model->load(Request::post())) {
            $model->is_changed = true;
            $model->save();
        }

        return $this->render('admin/update', ['model' => $model]);
    }
}