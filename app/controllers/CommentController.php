<?php

namespace app\controllers;

use app\core\Controller;
use app\core\Request;
use app\models\Comment;

/**
 * Class CommentController
 * @package app\controllers
 */
class CommentController extends Controller
{
    /**
     * Default action
     * @return string
     */
    public function actionIndex()
    {
        $sortAttribute = Request::get('sort_attr');
        $sortOrder = Request::get('sort_order');

        switch ($sortAttribute) {
            case 'name':
                $sortAttribute = 'name';
                break;
            case 'email':
                $sortAttribute = 'email';
                break;
            default:
                $sortAttribute = 'created_at';
        }

        switch (mb_strtolower($sortOrder)) {
            case 'asc':
                $sortOrder = 'ASC';
                break;
            default:
                $sortOrder = 'DESC';
        }

        $models = Comment::findAll([
            'status_key' => Comment::STATUS_ACCEPTED
        ], [
            $sortAttribute => $sortOrder
        ]);

        return $this->render('comment/index', ['models' => $models]);
    }

    /**
     * Create new comment
     */
    public function actionCreate()
    {
        $preview = Request::get('preview');

        $model = new Comment();
        if ($model->load(Request::post())) {

            $model->saveFile($preview);

            if ($model->validate() && !$preview) {
                $model->save();
            }
        }

        return json_encode([
            'name' => $model->name,
            'email' => $model->email,
            'text' => $model->text,
            'image_path' => $model->getImage(),
            'errors' => $model->getErrors()
        ]);
    }
}