<?php

namespace app\controllers;

use app\core\Controller;

/**
 * Class ErrorController
 * @package app\controllers
 */
class ErrorController extends Controller
{
    /**
     * @return string
     */
    public function action404()
    {
        return $this->render('error/404');
    }
}