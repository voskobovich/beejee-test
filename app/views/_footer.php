<!-- Footer -->
<footer style="margin-top: 100px">
    <div class="row">
        <div class="col-lg-12">
            <p>Copyright &copy; Vitaly Voskobovich 2016</p>
        </div>
    </div>
    <!-- /.row -->
</footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="/js/jquery.js"></script>
<script src="/js/jquery.tmpl.js"></script>
<script src="/js/main.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/js/bootstrap.min.js"></script>

</body>

</html>