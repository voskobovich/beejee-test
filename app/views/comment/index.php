<?php

use app\core\View;
use app\models\Comment;

/**
 * @var View $view
 * @var Comment[] $models
 */
?>

<?php include APP_PATH . '/views/_header.php' ?>

    <div style="margin-bottom: 30px">
        Сортировать по
        <form action="/" method="get">
            <select name="sort_attr">
                <option value="name">Имени</option>
                <option value="email">E-mail</option>
                <option value="created_at">Дате добавление</option>
            </select>
            <select name="sort_order">
                <option value="asc">Возрастание</option>
                <option value="desc">Убыване</option>
            </select>
            <input type="submit" class="btn btn-default" value="Применить">
            <a href="/" class="btn btn-default">Сбросить</a>
        </form>
    </div>

    <div id="comments_container">
        <?php foreach ($models as $model): ?>
            <div class="row comment_item js-comment_item">
                <div class="col-md-3">
                    <img class="img-responsive" src="<?= $model->getImage() ?>" alt="">
                </div>
                <div class="col-md-9">
                    <h3>Имя: <?= $model->name ?></h3>
                    <h4>E-mail: <?= $model->email ?></h4>
                    <?php if ($model->is_changed == 't'): ?>
                        <h5>Изменен администратором</h5>
                    <?php endif; ?>
                    <p><?= $model->text ?></p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="row" style="margin-top: 30px">
        <div class="col-lg-6">
            <form role="form" action="/comment/create" method="post" id="comment_form" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Имя:</label>
                    <input type="text" name="Comment[name]" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="email">E-email:</label>
                    <input type="text" name="Comment[email]" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="text">Сообщение:</label>
                    <textarea name="Comment[text]" class="form-control" id="text"></textarea>
                </div>
                <div class="form-group">
                    <label for="image">Изображение:</label>
                    <input type="file" id="image" name="Comment[image]">
                </div>

                <button type="button" data-preview="true"
                        class="btn btn-default js-comment_send-button">Предварительный просмотр
                </button>
                <button type="button" class="btn btn-default js-comment_send-button">Отправить</button>
            </form>
        </div>
    </div>

<?php include APP_PATH . '/views/_footer.php' ?>