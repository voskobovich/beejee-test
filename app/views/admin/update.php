<?php

use app\core\View;
use app\models\Comment;

/**
 * @var View $view
 * @var Comment $model
 */
?>

<?php include APP_PATH . '/views/_header.php' ?>

<a href="/admin/index" class="btn btn-default">Назад к списку</a>

<div class="row" style="margin-top: 30px">
    <div class="col-lg-6">
        <img class="img-responsive" src="<?= $model->getImage() ?>" alt="">

        <form role="form" action="" method="post">
            <div class="form-group">
                <label for="name">Имя:</label>
                <input type="text" name="Comment[name]" class="form-control" id="name" value="<?= $model->name ?>">
            </div>
            <div class="form-group">
                <label for="email">E-email:</label>
                <input type="text" name="Comment[email]" class="form-control" id="email" value="<?= $model->email ?>">
            </div>
            <div class="form-group">
                <label for="text">Сообщение:</label>
                <textarea name="Comment[text]" class="form-control" id="text"><?= $model->text ?></textarea>
            </div>
            <div class="form-group">
                <label for="status_key">Статус:</label>
                <select name="Comment[status_key]" class="form-control" id="status_key">
                    <?php foreach (Comment::getStatusItems() as $key => $name): ?>
                        <option <?= ($key == $model->status_key ? 'selected' : '') ?> value="<?= $key ?>">
                            <?= $name ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>

            <button type="submit" class="btn btn-default">Сохранить</button>
        </form>
    </div>
</div>

<?php include APP_PATH . '/views/_footer.php' ?>
