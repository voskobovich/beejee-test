<?php

use app\core\View;
use app\models\Comment;

/**
 * @var View $view
 * @var Comment[] $models
 */
?>

<?php include APP_PATH . '/views/_header.php' ?>

<?php foreach ($models as $model): ?>
    <div class="row comment_item js-comment_item">
        <div class="col-md-3">
            <img class="img-responsive" src="<?= $model->getImage() ?>" alt="">
        </div>
        <div class="col-md-9">
            <h3>Имя: <?= $model->name ?></h3>
            <h4>E-mail: <?= $model->email ?></h4>
            <h4>Статус: <?= $model->getStatus() ?></h4>
            <p><?= $model->text ?></p>
            <a href="/admin/update/<?= $model->id ?>" class="btn btn-default">Изменить</a>
        </div>
    </div>
<?php endforeach; ?>

<?php include APP_PATH . '/views/_footer.php' ?>