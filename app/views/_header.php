<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>BeeJee Test Application</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/css/1-col-portfolio.css" rel="stylesheet">

    <style type="text/css">
        .comment_item {
            padding: 5px 0;
        }

        .js-comment_item-preview .hint_moderated {
            display: none !important;
        }

        .js-comment_item-preview .hint_preview {
            display: inline !important;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script id="comment_template" type="text/template">
        <div class="row comment_item js-comment_item">
            <div class="col-md-3">
                <img class="img-responsive" src="${image_path}" alt="${name}">
            </div>
            <div class="col-md-9">
                <p class="hint hint_moderated" style="color: red">
                    <strong>Спасибо за отзыв!</strong> Запись появится после проверки администратором.
                </p>
                <p class="hint hint_preview" style="color: blue; display: none;">
                    Так будет выглядеть ваша запись после публикации.
                </p>
                <h3>${name}</h3>
                <h4>${email}</h4>
                <p>${text}</p>
            </div>
        </div>
    </script>
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Start Bootstrap</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/admin/index">Админпанель</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">BeeJee
                <small>Test Case</small>
            </h1>
        </div>
    </div>
    <!-- /.row -->