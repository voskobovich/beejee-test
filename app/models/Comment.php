<?php

namespace app\models;

use app\core\ActiveRecord;

/**
 * Class Comment
 * @package app\models
 */
class Comment extends ActiveRecord
{
    public $id;
    public $name;
    public $email;
    public $text;
    public $image_path;
    public $is_changed;
    public $status_key;
    public $created_at;
    public $updated_at;

    const STATUS_REJECTED = 0;
    const STATUS_ACCEPTED = 1;

    /**
     * Table name in DataBase
     * @return string
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * Validation rules
     */
    public function validate()
    {
        if (empty($this->name)) {
            $this->addError('name', 'Необходимо ввести имя.');
        } else {
            if (mb_strlen($this->name) > 255) {
                $this->addError('name', 'Имя не должно быть длиннее 255 символов.');
            }
        }

        if (empty($this->email)) {
            $this->addError('email', 'Необходимо ввести e-mail.');
        } else {
            if (mb_strlen($this->email) > 255) {
                $this->addError('email', 'E-mail не должен быть длиннее 255 символов.');
            }
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $this->addError('email', 'Значение "' . $this->email . '" не похоже на e-mail.');
            }
        }

        if (empty($this->text)) {
            $this->addError('text', 'Необходимо ввести сообщение.');
        }

        return !$this->hasErrors();
    }

    /**
     * @param null $key
     * @return array
     */
    public static function getStatusItems($key = null)
    {
        $items = [
            static::STATUS_ACCEPTED => 'Принят',
            static::STATUS_REJECTED => 'Отклонен'
        ];

        if (!is_null($key)) {
            return isset($items[$key]) ? $items[$key] : null;
        }

        return $items;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return static::getStatusItems($this->status_key);
    }

    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = time();
            }
            $this->updated_at = time();
            return true;
        }

        return false;
    }

    /**
     * Get image URL
     * @return string
     */
    public function getImage()
    {
        return $this->image_path ? $this->image_path : 'https://placehold.it/320x240';
    }

    /**
     * @param $preview
     * @return bool
     */
    public function saveFile($preview)
    {
        if (empty($_FILES['Comment'])) {
            return false;
        }

        $file = $_FILES['Comment'];

        if ($file['size']['image'] <= 0) {
            return false;
        }

        $type = $file['type']['image'];

        if (!in_array($type, ['image/jpeg', 'image/png', 'image/gif'])) {
            $this->addError('image_path', 'Допускается загружать только JPG, PNG, GIF изображения');
            return false;
        }

        $tempName = $file['tmp_name']['image'];
        $name = $file['name']['image'];

        if (!is_uploaded_file($tempName)) {
            return false;
        }

        switch ($type) {
            case 'image/png':
                $extension = 'png';
                break;
            case 'image/gif':
                $extension = 'gif';
                break;
            default:
                $extension = 'jpg';
        }

        if ($preview) {
            $newName = $name;
            $uploadPath = WEB_PATH . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'preview';
            $imageUrl = '/uploads/preview/' . $newName;
        } else {
            $newName = md5($name . microtime()) . '.' . $extension;
            $uploadPath = WEB_PATH . DIRECTORY_SEPARATOR . 'uploads';
            $imageUrl = '/uploads/' . $newName;
        }

        $fullPath = $uploadPath . DIRECTORY_SEPARATOR . $newName;
        if (move_uploaded_file($tempName, $fullPath)) {
            $this->image_path = $imageUrl;

            // Resize (only target image)
            if ($preview) {
                return true;
            }

            list($width, $height, $type) = getimagesize($fullPath);

            $maxWidth = 320;
            $maxHeight = 240;

            if ($width > $maxWidth || $height > $maxHeight) {
                if ($width >= $height) {
                    $ratio = $width / $maxWidth;
                    $newWidth = $maxWidth;
                    $newHeight = ceil($height / $ratio);
                } else {
                    $ratio = $height / $maxHeight;
                    $newHeight = $maxHeight;
                    $newWidth = ceil($width / $ratio);
                }

                switch ($type) {
                    case IMG_GIF:
                        $source = imagecreatefromgif($fullPath);
                        break;
                    case IMG_JPG:
                        $source = imagecreatefromjpeg($fullPath);
                        break;
                    case IMG_PNG:
                        $source = imagecreatefrompng($fullPath);
                        break;
                    case 3:
                        $source = imagecreatefrompng($fullPath);
                        break;
                }

                if (isset($source)) {
                    $destination = imagecreatetruecolor($newWidth, $newHeight);
                    imagecopyresampled($destination, $source, 0, 0, 0, 0, $newWidth, $newHeight,
                        $width, $height);

                    switch ($type) {
                        case IMG_GIF:
                            imagegif($destination, $fullPath);
                            break;
                        case IMG_JPG:
                            imagejpeg($destination, $fullPath, 100);
                            break;
                        case IMG_PNG:
                            imagepng($destination, $fullPath);
                            break;
                        case 3:
                            imagepng($destination, $fullPath);
                            break;
                    }

                    imagedestroy($source);
                    imagedestroy($destination);
                }
            }
        }

        return true;
    }
}