<?php

return [
    'components' => [
        'router' => [
            'class' => 'app\core\Router',
            'routes' => [
                'comment/([0-9]+)' => 'comment/view/$1',
                '404' => 'error/404',
                '' => 'comment/index',
            ]
        ],
        'db' => [
            'class' => 'app\core\DbConnection',
            'host' => '127.0.0.1',
            'db' => 'beejee',
            'user' => 'beejee',
            'pass' => 'beejee',
            'schema' => 'public'
        ]
    ]
];