<?php

namespace app\core;

/**
 * Class Object
 * @package app\core
 */
class Object
{
    /**
     * @param $object
     * @param $properties
     * @return mixed
     */
    public static function configure($object, $properties)
    {
        foreach ($properties as $name => $value) {
            $object->$name = $value;
        }

        return $object;
    }

    /**
     * Create and configure object
     * @param $config
     * @return mixed
     * @throws Exception
     */
    public function createObject($config)
    {
        if (empty($config['class'])) {
            throw new Exception('Key "class" can not be blank.');
        }

        $object = new $config['class']($config);
        unset($config['class']);
        return static::configure($object, $config);
    }
}