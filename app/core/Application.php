<?php

namespace app\core;

/**
 * Class Application
 * @package app\core
 */
class Application extends Component
{
    /**
     * List of components
     * @var array
     */
    protected $components = [];

    /**
     * @var Application
     */
    private static $_instance;

    /**
     * @param array $config
     * @return Application
     */
    public static function getInstance($config = [])
    {
        if (empty(self::$_instance)) {
            self::$_instance = new self($config);
        }

        return self::$_instance;
    }

    /**
     * Application constructor.
     * @param $config
     * @throws Exception
     */
    public function __construct($config)
    {
        foreach ($config['components'] as $componentName => $componentConfig) {
            $this->components[$componentName] = self::createObject($componentConfig);
        }
    }

    /**
     * Get component by name
     * @param $name
     * @return Component
     * @throws Exception
     */
    public function get($name)
    {
        if (!isset($this->components[$name])) {
            throw new Exception('Component is not defined.');
        }

        return $this->components[$name];
    }

    /**
     * Run application
     */
    public function run()
    {
        $this->get('router')->run();
    }
}