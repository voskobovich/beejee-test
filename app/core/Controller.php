<?php

namespace app\core;

/**
 * Class Controller
 * @package app\core
 */
class Controller
{
    /**
     * @var View
     */
    protected $view;

    /**
     * Controller constructor.
     */
    function __construct()
    {
        $this->view = new View();
    }

    /**
     * Get view
     * @return View
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * Render view file
     * @param $view
     * @param array $params
     * @return string
     */
    public function render($view, $params = [])
    {
        return $this->getView()->render($view, $params);
    }
}