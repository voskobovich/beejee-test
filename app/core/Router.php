<?php

namespace app\core;

use app\core\interfaces\RouterInterface;

/**
 * Class Router
 * @package app\core
 */
class Router implements RouterInterface
{
    /**
     * Routes list
     * @var array
     */
    public $routes;

    /**
     * Get URI form request
     * @return string
     */
    public function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }

        if (!empty($_SERVER['QUERY_STRING'])) {
            return trim($_SERVER['QUERY_STRING'], '/');
        }

        return '';
    }

    /**
     * Run router
     */
    function run()
    {
        $uri = $this->getURI();
        if (($pos = mb_strpos($uri, '?')) !== false) {
            $uri = mb_substr($uri, 0, $pos);
        }

        // Check all rules
        foreach ($this->routes as $pattern => $route) {
            if ($pattern != '' && preg_match("~$pattern~", $uri)) {
                $internalRoute = preg_replace("~$pattern~", $route, $uri);
                $segments = explode('/', $internalRoute);
                $controller = array_shift($segments);
                $action = array_shift($segments);
                $parameters = $segments;

                $this->runController($controller, $action, $parameters);
                return;
            }
        }

        // Maybe URI = controller/name ?
        if ($uri != '') {
            $segments = explode('/', $uri);
        } elseif (!empty($this->routes[''])) {
            $internalRoute = $this->routes[''];
            $segments = explode('/', $internalRoute);
        }

        $controller = array_shift($segments);
        $action = array_shift($segments);
        $parameters = $segments;


        $this->runController($controller, $action, $parameters);
        return;
    }

    /**
     * @param $controllerName
     * @param string $actionName
     * @param array $parameters
     */
    private function runController($controllerName = 'default', $actionName = 'index', $parameters = [])
    {
        $controller = 'app\\controllers\\' . ucfirst($controllerName) . 'Controller';
        $action = 'action' . ucfirst($actionName);

        if (!is_callable([$controller, $action])) {
            $this->error404();
            return;
        }

        try {
            echo call_user_func_array([new $controller, $action], $parameters);
        } catch (HttpException $ex) {
            $this->error404($ex->getMessage());
        }
    }

    /**
     * Send 404 response code
     * @param string $message
     */
    private function error404($message = 'Page not found')
    {
        header("HTTP/1.0 404 {$message}");
        header('Location: /404');
    }
}