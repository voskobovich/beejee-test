<?php

namespace app\core;

/**
 * Class DB
 * @package app\core
 */
class DbConnection extends Component
{
    public $host;
    public $user;
    public $pass;
    public $db;
    public $schema;

    private $connection;

    /**
     * DB constructor.
     * @param array $params
     */
    function __construct($params = [])
    {
        self::configure($this, $params);

        $this->open();
    }

    /**
     * Open connection
     */
    public function open()
    {
        $connectionParams = "host={$this->host} dbname={$this->db} user={$this->user} password={$this->pass}";
        $this->connection = pg_connect($connectionParams) or die ('Could not connect to server' . PHP_EOL);

        $this->execute('SET search_path TO ' . $this->schema);
    }

    /**
     * Close connection
     */
    public function close()
    {
        pg_close($this->connection);
    }

    /**
     * @param $sql
     * @return resource
     * @throws DbException
     */
    public function execute($sql)
    {
        $resource = pg_query($this->connection, $sql);

        if (!$resource) {
            $this->throwException();
        }

        return $resource;
    }

    /**
     * @param $sql
     * @return array
     * @throws DbException
     */
    public function select($sql)
    {
        $resource = $this->execute($sql);

        $rows = [];
        while ($row = pg_fetch_row($resource)) {
            array_push($rows, $row);
        }

        return $rows;
    }

    /**
     * @param $tableName
     * @return array
     * @throws DbException
     */
    public function getTableColumns($tableName)
    {
        $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '{$tableName}' AND table_schema = '{$this->schema}';";
        $resource = $this->execute($sql);

        $rows = [];
        while ($row = pg_fetch_row($resource)) {
            array_push($rows, $row);
        }

        return $rows;
    }

    /**
     * @throws DbException
     */
    public function throwException()
    {
        throw new DbException(pg_last_error($this->connection));
    }

    /**
     * @param $where
     * @return string
     */
    public function buildWhere($where)
    {
        if (is_array($where)) {
            $tempWhere = '';

            foreach ($where as $whereColumn => $whereValue) {
                $whereValue = $this->escapeString($whereValue);
                $tempWhere .= "{$whereColumn} = {$whereValue} AND ";
            }

            $where = rtrim($tempWhere, ' AND ');
        }

        if (!empty($where)) {
            $where = ' WHERE ' . $where;
        }

        return $where;
    }

    /**
     * @param $order
     * @return string
     */
    public function buildOrder($order)
    {
        if (is_array($order)) {
            $tempOrder = '';

            foreach ($order as $whereColumn => $whereValue) {
                $tempOrder .= "{$whereColumn} {$whereValue}, ";
            }

            $order = rtrim($tempOrder, ', ');
        }

        if (!empty($order)) {
            $order = ' ORDER BY ' . $order;
        }

        return $order;
    }

    /**
     * @param $value
     * @return string
     */
    function escapeString($value)
    {
        $value = strip_tags($value);
        $value = htmlspecialchars($value);
        return "'" . pg_escape_string($this->connection, $value) . "'";
    }
}