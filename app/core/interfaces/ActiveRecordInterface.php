<?php

namespace app\core\interfaces;

/**
 * Interface ActiveRecordInterface
 * @package app\core\interfaces
 */
interface ActiveRecordInterface
{
    /**
     * Return table name in DB
     * @return mixed
     */
    public static function tableName();

    /**
     * Validation rules
     * @return mixed
     */
    public function validate();

    /**
     * Get primary key value
     * @return mixed
     */
    public function getPrimaryKey();

    /**
     * Get primary key column name
     * @return string
     */
    public function primaryKey();
}