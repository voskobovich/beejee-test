<?php

namespace app\core\interfaces;

/**
 * Interface RouterInterface
 * @package app\core\interfaces
 */
interface RouterInterface
{
    public function run();
}