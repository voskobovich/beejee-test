<?php

namespace app\core;

/**
 * Class View
 * @package app\core
 */
class View extends Component
{
    /**
     * @param $view
     * @param array $params
     * @return string
     * @throws Exception
     */
    public function render($view, $params = [])
    {
        $viewFile = APP_PATH . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $view . '.php';
        $params['view'] = $this;

        if (!file_exists($viewFile)) {
            throw new Exception('View "' . $view . '" not found. Full path: ' . $viewFile);
        }

        ob_start();
        ob_implicit_flush(false);
        extract($params, EXTR_OVERWRITE);
        require($viewFile);
        return ob_get_clean();
    }
}