<?php

namespace app\core;

use app\core\interfaces\ActiveRecordInterface;

/**
 * Class ActiveRecord
 * @package app\core
 */
abstract class ActiveRecord extends Model implements ActiveRecordInterface
{
    /**
     * @var bool
     */
    public $isNewRecord = true;

    /**
     * Model DB connection
     * @return DbConnection
     */
    public static function getDb()
    {
        return Application::getInstance()->get('db');
    }

    /**
     * Get primary key value
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->{$this->primaryKey()};
    }

    /**
     * Get primary key column name
     * @return string
     */
    public function primaryKey()
    {
        return 'id';
    }

    /**
     * @param string $where
     * @param string $order
     * @return array
     */
    public static function findAll($where = '', $order = '')
    {
        $where = static::getDb()->buildWhere($where);
        $order = static::getDb()->buildOrder($order);

        $dbRows = self::getDb()->select('SELECT * FROM "' . static::tableName() . '"' . $where . $order);
        $tableColumns = self::getDb()->getTableColumns(static::tableName());

        $models = [];
        foreach ($dbRows as $dbRow) {
            $model = new static();
            $model->isNewRecord = false;

            foreach ($dbRow as $n => $dbColumnValue) {
                $model->setAttribute($tableColumns[$n][0], $dbColumnValue);
            }

            array_push($models, $model);
        }

        return $models;
    }

    /**
     * @param string $where
     * @return null|static
     */
    public static function findOne($where = '')
    {
        $where = static::getDb()->buildWhere($where);

        $dbRows = self::getDb()->select('SELECT * FROM "' . static::tableName() . '"' . $where);
        $tableColumns = self::getDb()->getTableColumns(static::tableName());

        if (empty($dbRows)) {
            return null;
        }

        $model = new static();
        $model->isNewRecord = false;
        foreach ($dbRows[0] as $n => $dbColumnValue) {
            $model->setAttribute($tableColumns[$n][0], $dbColumnValue);
        }

        return $model;
    }

    /**
     * @return bool
     */
    public function save()
    {
        $this->validate();

        if ($this->hasErrors()) {
            return false;
        }

        if (!$this->beforeSave($this->isNewRecord)) {
            return false;
        }

        if ($this->isNewRecord) {
            return $this->insert();
        } else {
            return $this->update();
        }
    }

    /**
     * Insert new record
     * @return bool
     */
    public function insert()
    {
        $tableName = static::tableName();
        $tableColumns = self::getDb()->getTableColumns($tableName);

        $rows = [];
        foreach ($tableColumns as $tableColumn) {
            $attributeName = $tableColumn[0];
            $value = $this->getAttributeValue($attributeName);
            if ($value) {
                $rows[$attributeName] = static::getDb()->escapeString($value);
            }
        }

        $columns = array_keys($rows);
        $columnsString = '';
        $valuesString = '';
        foreach ($columns as $column) {
            $columnsString .= "{$column}, ";
            $valuesString .= "{$rows[$column]}, ";
        }
        $columnsString = mb_substr($columnsString, 0, mb_strlen($columnsString) - 2);
        $valuesString = mb_substr($valuesString, 0, mb_strlen($valuesString) - 2);

        $sql = "INSERT INTO \"{$tableName}\" ({$columnsString}) VALUES ({$valuesString})";

        return (bool)static::getDb()->execute($sql);
    }

    /**
     * Update record
     * @return bool
     */
    public function update()
    {
        $tableName = static::tableName();
        $tableColumns = self::getDb()->getTableColumns($tableName);

        $rows = [];
        foreach ($tableColumns as $tableColumn) {
            $attributeName = $tableColumn[0];
            $value = $this->getAttributeValue($attributeName);
            if ($value !== null) {
                $rows[$attributeName] = static::getDb()->escapeString($value);
            }
        }

        $setString = '';
        foreach ($rows as $rowName => $rowValue) {
            $setString .= "{$rowName} = {$rowValue}, ";
        }
        $setString = mb_substr($setString, 0, mb_strlen($setString) - 2);

        $id = static::getDb()->escapeString($this->getPrimaryKey());
        $sql = "UPDATE \"{$tableName}\" SET {$setString} WHERE id = {$id}";

        return (bool)static::getDb()->execute($sql);
    }

    /**
     * @return bool
     */
    public function beforeSave($insert)
    {
        return true;
    }

}