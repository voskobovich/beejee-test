<?php

namespace app\core;

/**
 * Class Request
 * @package app\core
 */
class Request extends Component
{
    /**
     * @param null $key
     * @param null $defaultValue
     * @return mixed
     */
    public static function get($key = null, $defaultValue = null)
    {
        $params = $_GET;

        if ($key !== null) {
            return isset($params[$key]) ? $params[$key] : $defaultValue;
        }

        return $params;
    }

    /**
     * @param null $key
     * @param null $defaultValue
     * @return mixed
     */
    public static function post($key = null, $defaultValue = null)
    {
        $params = $_POST;

        if ($key !== null) {
            return isset($params[$key]) ? $params[$key] : $defaultValue;
        }

        return $params;
    }
}