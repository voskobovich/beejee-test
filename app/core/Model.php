<?php

namespace app\core;

/**
 * Class Model
 * @package app\core
 */
abstract class Model extends Component
{
    /**
     * Validate errors
     * @var array
     */
    protected $errors = [];

    /**
     * @param $name
     * @param $value
     */
    public function setAttribute($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        }
    }

    /**
     * @param $name
     * @return null
     */
    public function getAttributeValue($name)
    {
        return isset($this->$name) ? $this->$name : null;
    }

    /**
     * @param $params
     * @param null $formName
     * @return bool
     */
    public function load($params, $formName = null)
    {
        $scope = $formName === null ? $this->formName() : $formName;

        if (empty($params[$scope])) {
            return false;
        }

        foreach ($params[$scope] as $paramName => $paramValue) {
            $this->setAttribute($paramName, $paramValue);
        }

        return true;
    }

    /**
     * @return string
     */
    public function formName()
    {
        $reflector = new \ReflectionClass($this);
        return $reflector->getShortName();
    }

    /**
     * @param $name
     * @param $message
     */
    public function addError($name, $message)
    {
        $errors = isset($this->errors[$name]) ? $this->errors[$name] : [];
        array_push($errors, $message);
        $this->errors[$name] = $errors;
    }

    /**
     * @return bool
     */
    public function hasErrors()
    {
        return !empty($this->errors);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}