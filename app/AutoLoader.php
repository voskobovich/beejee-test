<?php

/**
 * Class AutoLoader
 */
final class AutoLoader
{
    /**
     * Load class by class name
     * @param $className
     */
    public static function loadClass($className)
    {
        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';

        require ROOT_PATH . DIRECTORY_SEPARATOR . $fileName;
    }
}